from pprint import pprint
import requests
from parsel import Selector
from dp.motor import cars, create_tables, init_db

MAIN_URL = "https://cars.kg/offers"


def get_html(url: str):
    response = requests.get(url)
    if response.status_code == 200:
        return response.text
    else:
        raise Exception("Страница не загружается")


def parse_html(html: str):
    selector = Selector(text=html)
    return selector


def clean_text(text: str):
    if text is None:
        return ''
    text = ' '.join(text.split())
    text = text.replace('\n', ' ').replace('\r', ' ').replace('\t', ' ').replace(',', '')
    return text


def clean_text_list(lst: list):
    if lst is None:
        return ''
    big_list = []
    for txt in lst:
        big_list.append(clean_text(txt))
    return ''.join(big_list)


def get_cars_info(selector: Selector):
    info_cars = []
    for car in selector.css('.catalog-list .catalog-list-item'):
        car_info = {}
        car_info['title'] = clean_text(car.css('.catalog-item-caption::text').get())
        car_info['url'] = f"{MAIN_URL}{car.css('a').attrib.get('href')}"
        price_usd = car.css('.catalog-item-price::text').get()
        car_info['price_usd'] = price_usd.replace('$', '').strip()
        car_info['description'] = clean_text_list(car.css('.catalog-item-descr::text').getall())
        info_cars.append(car_info)
    return info_cars


def main():
    init_db()
    create_tables()
    html = get_html(f"{MAIN_URL}")
    selector = parse_html(html)
    info_cars = get_cars_info(selector)
    pprint(info_cars)
    for car_info in info_cars:
        cars(car_info)


if __name__ == '__main__':
    main()
