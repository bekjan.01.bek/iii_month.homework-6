import sqlite3
from pathlib import Path

def init_db():
    global db, cursor
    DB_PATH = Path(__file__).parent.parent
    DB_NAME = 'db.sqlite'
    db = sqlite3.connect(DB_PATH / DB_NAME)
    cursor = db.cursor()

def create_tables():
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS cars(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            description TEXT,
            price_usd INTEGER,
            title TEXT,
            url TEXT
        )
        """)
    db.commit()

def populate_tables():
    cursor.execute("""
        INSERT INTO cars(title, price_usd, description)
        VALUES  ('Mercedes-Benz E-Класс', '75000', 'Седан, черный, 2 л., левый руль, бензин, автомат, задний'),
                ('Toyota Camry', '36500', 'Седан, черный, 2.5 л., левый руль, бензин, автомат, передний')
    """)
    db.commit()

def cars(data):
    cursor.execute(
        """
        INSERT INTO cars (title, price_usd, description)
        VALUES (:title, :price_usd, :description)
        """,
        {'title': data['title'],
         'price_usd': data['price_usd'],
         'description': data['description']}
    )
    db.commit()


